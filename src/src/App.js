import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './App.css';

class MyFakeComponent extends Component {

  componentWillMount() {
    console.log('Fake Will Mount', Date.now());
  }

  componentDidMount() {
    console.log('Fake has finished mounting', Date.now())
  }

  componentWillUnmount() {
    console.log('Fake Will Unmount');
  }
  
  componentDidUpdate(prevProps, prevState) {
    console.log('Fake updated', Date.now());
    console.log('Previous props', prevProps);
    console.log('Current props', this.props);
    console.log('-------------');
    console.log('Previous state', prevState);
    console.log('Current state', this.state);
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('Fake is checking if he should update', Date.now());
    if ((nextProps.myProp > 10) && (this.props.myProp > 10)) {
      return false;
    }

    if ((nextProps.myProp <= 10) && (this.props.myProp <= 10)) {
      return false;
    }

    return true;
  }

  componentWillReceiveProps(newProps) {
    console.log('Fake receiving props', Date.now());
  }
  
  componentWillUpdate(nextProps, nextState) {
    console.log('Fake will be updating', Date.now());
  }

  render() {
    console.log('Fake is rendering', Date.now())
    let msg = ' and goodbye';
    if (this.props.myProp > 10) {
      msg = '... still here';
    }
    return <div>Hello World {msg}</div>;
  }
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      counterValue: props.counterInitialValue,
    };

    this.decrementValue = this.decrementValue.bind(this);
    this.incrementValue = this.incrementValue.bind(this);
  }

  static defaultProps = {
    counterInitialValue: 0,
  }

  static propTypes = {
    counterInitialValue: PropTypes.number,
  }

  decrementValue() {
    this.setState((prevState) => {
      return {
        counterValue: prevState.counterValue - 1,
      };
    });
  }

  incrementValue() {
    this.setState((prevState) => {
      return {
        counterValue: prevState.counterValue + 1,
      };
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to JS Demo</h1>
        </header>
        <p className="App-intro">
          <button onClick={this.decrementValue}>-</button>
          <input
            type="text"
            value={this.state.counterValue}
          />
          <button onClick={this.incrementValue}>+</button>
        </p>
        {this.state.counterValue > 5 ? <MyFakeComponent myProp={this.state.counterValue} /> : null}
      </div>
    );
  }
}

export default App;
